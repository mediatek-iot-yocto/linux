// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright (C) 2022 MediaTek Inc.
 */

#include "mt8195.dtsi"

/ {
	soc {
		xhci0: usb@11200000 {
			compatible = "mediatek,mt8195-xhci",
				     "mediatek,mtk-xhci";
			reg = <0 0x11200000 0 0x1000>,
			      <0 0x11203e00 0 0x0100>;
			reg-names = "mac", "ippc";
			interrupts = <GIC_SPI 129 IRQ_TYPE_LEVEL_HIGH 0>;
			phys = <&u2port0 PHY_TYPE_USB2>,
			       <&u3port0 PHY_TYPE_USB3>;
			assigned-clocks = <&topckgen CLK_TOP_USB_TOP>,
					  <&topckgen CLK_TOP_SSUSB_XHCI>;
			assigned-clock-parents = <&topckgen CLK_TOP_UNIVPLL_D5_D4>,
						 <&topckgen CLK_TOP_UNIVPLL_D5_D4>;
			clocks = <&infracfg_ao CLK_INFRA_AO_SSUSB>,
				 <&topckgen CLK_TOP_SSUSB_REF>,
				 <&apmixedsys CLK_APMIXED_USB1PLL>,
				 <&clk26m>,
				 <&infracfg_ao CLK_INFRA_AO_SSUSB_XHCI>;
			clock-names = "sys_ck", "ref_ck", "mcu_ck", "dma_ck",
				      "xhci_ck";
			mediatek,syscon-wakeup = <&pericfg 0x400 103>;
			wakeup-source;
			status = "disabled";
		};

		xhci1: usb@11290000 {
			compatible = "mediatek,mt8195-xhci",
				     "mediatek,mtk-xhci";
			reg = <0 0x11290000 0 0x1000>,
			      <0 0x11293e00 0 0x0100>;
			reg-names = "mac", "ippc";
			interrupts = <GIC_SPI 530 IRQ_TYPE_LEVEL_HIGH 0>;
			phys = <&u2port1 PHY_TYPE_USB2>;
			assigned-clocks = <&topckgen CLK_TOP_USB_TOP_1P>,
					  <&topckgen CLK_TOP_SSUSB_XHCI_1P>;
			assigned-clock-parents = <&topckgen CLK_TOP_UNIVPLL_D5_D4>,
						 <&topckgen CLK_TOP_UNIVPLL_D5_D4>;
			clocks = <&pericfg_ao CLK_PERI_AO_SSUSB_1P_BUS>,
				 <&topckgen CLK_TOP_SSUSB_P1_REF>,
				 <&apmixedsys CLK_APMIXED_USB1PLL>,
				 <&clk26m>,
				 <&pericfg_ao CLK_PERI_AO_SSUSB_1P_XHCI>;
			clock-names = "sys_ck", "ref_ck", "mcu_ck", "dma_ck",
				      "xhci_ck";
			mediatek,syscon-wakeup = <&pericfg 0x400 104>;
			wakeup-source;
			status = "disabled";
		};

		xhci2: usb@112a0000 {
			compatible = "mediatek,mt8195-xhci",
				     "mediatek,mtk-xhci";
			reg = <0 0x112a0000 0 0x1000>,
			      <0 0x112a3e00 0 0x0100>;
			assigned-clock-parents = <&topckgen CLK_TOP_UNIVPLL_D5_D4>,
						 <&topckgen CLK_TOP_UNIVPLL_D5_D4>;
			clocks = <&pericfg_ao CLK_PERI_AO_SSUSB_2P_BUS>,
				 <&topckgen CLK_TOP_SSUSB_P2_REF>,
				 <&clk26m>,
				 <&clk26m>,
				 <&pericfg_ao CLK_PERI_AO_SSUSB_2P_XHCI>;
			clock-names = "sys_ck", "ref_ck", "mcu_ck", "dma_ck",
				      "xhci_ck";
			mediatek,syscon-wakeup = <&pericfg 0x400 105>;
			wakeup-source;
			status = "disabled";
		};

		xhci3: usb@112b0000 {
			compatible = "mediatek,mt8195-xhci",
				     "mediatek,mtk-xhci";
			reg = <0 0x112b0000 0 0x1000>,
			      <0 0x112b3e00 0 0x0100>;
			reg-names = "mac", "ippc";
			interrupts = <GIC_SPI 536 IRQ_TYPE_LEVEL_HIGH 0>;
			phys = <&u2port3 PHY_TYPE_USB2>;
			assigned-clocks = <&topckgen CLK_TOP_USB_TOP_3P>,
					  <&topckgen CLK_TOP_SSUSB_XHCI_3P>;
			assigned-clock-parents = <&topckgen CLK_TOP_UNIVPLL_D5_D4>,
						 <&topckgen CLK_TOP_UNIVPLL_D5_D4>;
			clocks = <&pericfg_ao CLK_PERI_AO_SSUSB_3P_BUS>,
				 <&topckgen CLK_TOP_SSUSB_P3_REF>,
				 <&clk26m>,
				 <&clk26m>,
				 <&pericfg_ao CLK_PERI_AO_SSUSB_3P_XHCI>;
			clock-names = "sys_ck", "ref_ck", "mcu_ck", "dma_ck",
				      "xhci_ck";
			mediatek,syscon-wakeup = <&pericfg 0x400 106>;
			wakeup-source;
			status = "disabled";
		};
	};
};
