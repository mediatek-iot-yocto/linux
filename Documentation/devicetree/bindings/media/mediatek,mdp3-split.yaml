# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/media/mediatek,mdp3-split.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: MediaTek Media Data Path 3 SPLIT Device Tree Bindings

maintainers:
  - Matthias Brugger <matthias.bgg@gmail.com>
  - Moudy Ho <moudy.ho@mediatek.com>

description: |
  One of Media Data Path 3 (MDP3) components used to split hdmi rx into two stream

properties:
  compatible:
    enum:
      - mediatek,mt8195-mdp3-split

  reg:
    maxItems: 1

  mediatek,gce-client-reg:
    $ref: /schemas/types.yaml#/definitions/phandle-array
    items:
      items:
        - description: phandle of GCE
        - description: GCE subsys id
        - description: register offset
        - description: register size
    description: The register of client driver can be configured by gce with
      4 arguments defined in this property. Each GCE subsys id is mapping to
      a client defined in the header include/dt-bindings/gce/<chip>-gce.h.

  clocks:
    items:
      - description: Engine Clock
      - description: HDMI Clock
      - description: MDHI Clock
      - description: Digital Clock
      - description: Digital Clock
      - description: Digital Clock
      - description: HS Clock

  power-domains:
    maxItems: 1

required:
  - compatible
  - reg
  - mediatek,gce-client-reg
  - clocks
  - power-domains

additionalProperties: false

examples:
  - |
    #include <dt-bindings/clock/mt8195-clk.h>
    #include <dt-bindings/gce/mt8195-gce.h>
    #include <dt-bindings/power/mt8195-power.h>

    mdp3-split0@14f06000 {
      compatible = "mediatek,mt8195-mdp3-split";
      reg = <0x14f06000 0x1000>;
      mediatek,gce-client-reg = <&gce1 SUBSYS_14f0XXXX 0x6000 0x1000>;
      clocks = <&vppsys1 CLK_VPP1_VPP_SPLIT>,
               <&vppsys1 CLK_VPP1_HDMI_META>,
               <&vppsys1 CLK_VPP1_VPP_SPLIT_HDMI>,
               <&vppsys1 CLK_VPP1_DGI_IN>,
               <&vppsys1 CLK_VPP1_DGI_OUT>,
               <&vppsys1 CLK_VPP1_VPP_SPLIT_DGI>,
               <&vppsys1 CLK_VPP1_VPP_SPLIT_26M>;
      power-domains = <&spm MT8195_POWER_DOMAIN_VPPSYS0>;
    };
