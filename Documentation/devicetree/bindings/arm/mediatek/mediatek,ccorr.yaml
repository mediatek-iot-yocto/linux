# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/arm/mediatek/mediatek,ccorr.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Mediatek color correction

maintainers:
  - Matthias Brugger <matthias.bgg@gmail.com>

description: |
  Mediatek color correction with 3X3 matrix.

properties:
  compatible:
    items:
      - enum:
        - mediatek,mt8183-mdp3-ccorr

  mediatek,mdp3-id:
    $ref: /schemas/types.yaml#/definitions/uint32
    maxItems: 1
    description: |
      There may be multiple blocks with the same function but
      different addresses in MDP3.
      In order to distinguish the connection with other blocks,
      a unique ID is needed to dynamically use one or more identical
      blocks to implement multiple pipelines.

  reg:
    maxItems: 1

  mediatek,gce-client-reg:
    $ref: /schemas/types.yaml#/definitions/phandle-array
    description: |
      The register of client driver can be configured by gce with 4 arguments
      defined in this property, such as phandle of gce, subsys id,
      register offset and size.
      Each GCE subsys id is mapping to a client defined in the header
      include/dt-bindings/gce/<chip>-gce.h.

  clocks:
    minItems: 1

additionalProperties: false

examples:
  - |
    #include <dt-bindings/clock/mt8183-clk.h>
    #include <dt-bindings/gce/mt8183-gce.h>

    mdp3_ccorr: mdp3_ccorr@1401c000 {
      compatible = "mediatek,mt8183-mdp3-ccorr";
      mediatek,mdp3-id = <0>;
      reg = <0x1401c000 0x1000>;
      mediatek,gce-client-reg = <&gce SUBSYS_1401XXXX 0xc000 0x1000>;
      clocks = <&mmsys CLK_MM_MDP_CCORR>;
    };
