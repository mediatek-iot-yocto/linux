# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/arm/mediatek/mediatek,wdma.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Mediatek Write Direct Memory Access

maintainers:
  - Matthias Brugger <matthias.bgg@gmail.com>

description: |
  Mediatek Write Direct Memory Access(WDMA) component used to write
  the data into DMA.

properties:
  compatible:
    items:
      - enum:
        - mediatek,mt8183-mdp3-wdma

  mediatek,mdp3-id:
    $ref: /schemas/types.yaml#/definitions/uint32
    maxItems: 1
    description: |
      There may be multiple blocks with the same function but
      different addresses in MDP3.
      In order to distinguish the connection with other blocks,
      a unique ID is needed to dynamically use one or more identical
      blocks to implement multiple pipelines.

  reg:
    maxItems: 1

  mediatek,gce-client-reg:
    $ref: /schemas/types.yaml#/definitions/phandle-array
    description: |
      The register of client driver can be configured by gce with 4 arguments
      defined in this property, such as phandle of gce, subsys id,
      register offset and size.
      Each GCE subsys id is mapping to a client defined in the header
      include/dt-bindings/gce/<chip>-gce.h.

  power-domains:
    maxItems: 1

  clocks:
    minItems: 1

  iommus:
    maxItems: 1

additionalProperties: false

examples:
  - |
    #include <dt-bindings/clock/mt8183-clk.h>
    #include <dt-bindings/gce/mt8183-gce.h>
    #include <dt-bindings/power/mt8183-power.h>
    #include <dt-bindings/memory/mt8183-larb-port.h>

    mdp3_wdma: mdp3_wdma@14006000 {
      compatible = "mediatek,mt8183-mdp3-wdma";
      mediatek,mdp3-id = <0>;
      reg = <0x14006000 0x1000>;
      mediatek,gce-client-reg = <&gce SUBSYS_1400XXXX 0x6000 0x1000>;
      power-domains = <&spm MT8183_POWER_DOMAIN_DISP>;
      clocks = <&mmsys CLK_MM_MDP_WDMA0>;
      iommus = <&iommu>;
    };
