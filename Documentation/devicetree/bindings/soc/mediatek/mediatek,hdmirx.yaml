# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/display/mediatek/mediatek,hdmirx.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: mediatek hdmirx Device Tree Bindings

maintainers:
  - Bo xu <bo.xu@mediatek.com>

description: |
  The MediaTek hdmirx function block is a HDMI2.0 sink and
  Provide maximum 4K60/50 RGB/YUV444/YUV422 24bits and
  4K60/50 YUV420 48bits HDMI input.

properties:
  compatible:
    enum:
      - mediatek,mt8195-hdmirx

  clocks:
    items:
      - description: HDMI bus clock
      - description: HDMI 208mHZ clock
      - description: HDCP engine clock
      - description: HDCP 104mHZ clock
      - description: HDMI clock

  power-domains:
    description: |
      It is the control of hdmirx power domain.

  clock-names:
    items:
      - const: hdmiclksel
      - const: hdmiclk208m
      - const: hdcpclksel
      - const: hdcpclk104m
      - const: hdmisel

  reg:
    maxItems: 9

  interrupts:
    maxItems: 2

required:
  - compatible
  - clocks
  - power-domains
  - clock-names
  - reg
  - interrupts

additionalProperties: false

examples:
  - |
    #include <dt-bindings/interrupt-controller/arm-gic.h>
    #include <dt-bindings/clock/mt8173-clk.h>
    #include <dt-bindings/interrupt-controller/arm-gic.h>
    #include <dt-bindings/interrupt-controller/irq.h>
    hdmirx0: hdmirx@1c400000 {
        compatible = "mediatek,mt8195-hdmirx";
        clocks = <&topckgen CLK_TOP_HDMI_APB>,
             <&topckgen CLK_TOP_MSDCPLL_D2>,
             <&topckgen CLK_TOP_HD20_HDCP_CCLK>,
             <&topckgen CLK_TOP_MSDCPLL_D4>,
             <&topckgen CLK_TOP_HDMI_XTAL>;
        power-domains = <&spm MT8195_POWER_DOMAIN_HDMI_RX>;
        clock-names = "hdmiclksel",
             "hdmiclk208m",
             "hdcpclksel",
             "hdcpclk104m",
             "hdmisel";
        reg = <0 0x1c400000 0 0x1000>,
             <0 0x1c300000 0 0x1000>,
             <0 0x11d5f000 0 0x1000>,
             <0 0x11d60000 0 0x3000>,
             <0 0x10014800 0 0x0400>,
             <0 0x10014400 0 0x0400>,
             <0 0x1000c000 0 0x1000>,
             <0 0x10000000 0 0x1000>,
             <0 0x10007000 0 0x1000>;
        interrupts = <GIC_SPI 678 IRQ_TYPE_LEVEL_HIGH 0>,
             <GIC_SPI 823 IRQ_TYPE_LEVEL_HIGH 0>;
    };

...
